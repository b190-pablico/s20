console.log("Hello World");

/*let count = 5;*/

// While Loop
/*
	-takes in an expression/condition
	-expressions are any unit of code that can be evaluated to a value
	-if the evaluation of the condition is true, the statements will be executed.
	-a loop will iterate a certain number of times until the expression is/is not met.
	-"iteration" is the term given to the repetition of the statement/s

	SYNTAX:
		while( expression/condition ){
			statement/s;
		}
*/
/*while (count !== 0) {
	console.log("While loop result: " + count);
	count--;
};*/

// miniactivity

/*count = 0;
// checks for the value of count
while (count <= 10){
	// the main statement to be performed if the expression remains true
	console.log("While loop result: " + count)

	// changes the value of the count variable, serves as the loop breaker
	count++;
}
*/
// Do-while loop
/*
	-a do-while loop works a lot like the while loop. but, unlike while loop, do-while loops guarantee that the code will be executed at least once.
	SYNTAX:
		do{
			statment/s
		}while
*/
/*
	-Number function works similarly to the "parseInt" function
	-both differ significantly in terms of the process they undertake in converting information into a number data type.
	-How do-while loop works:
		1. the statements in "do" block executes once
		2. the message in the statements will be executed
		3. after executing once, the while statement will evaluate whether to run the uteration of the loop or not based on the expression(if the number is less than 10)
		4. if the condition is true, an iteration will be done
		5. if the condition is false, the loop will stop
*/
/*let number = Number(prompt("Give me a number"));
do{
	console.log("Do-while loop: " + number);

	number+=1;
} while (number < 10);
*/
// For loop
/*
	-the most flexible looping compared to do-while and while loops. it has 3 parts
	 	1. initialization - tracks the progression of the loop
	 	2. condition/expression - will be evaluated while will determine whetther the loop will run one more time.
	 	3. finalExpression - indicates how to advance the loop

	 -Stages of For Loop
	 	-will initialize a variable "count" that has the value of 0;
	 	-the condition/expression that is to be assessed is if the value of "count" is less than or equal to 20
	 	-perform the statements should the condition/expression returns true
	 	-increment the value of count
*/
for (let count = 0; count <= 20; count++){
	console.log("For Loop: " + count)
}

// using strings
/*
	characters in a string may be counted using the .length property. the property measures the number of elements (characters), not the string itself. it returns a number

	
	

	

*/
let myString = "supercalifragilisticexpiliadocious";
console.log(myString);

/*
	the function below looks like we are performing the following. these are also examples on how we can get access to the characters inside the string itself
		consol.log(myString[0]);
		consol.log(myString[1]);
		consol.log(myString[2]);
		consol.log(myString[3]);
*/
for (let x = 0 ; x < myString.length; x++) {
	console.log(myString[x]);
};

// miniactivity

/*let myName = "christopher"

for (let x = 0; x < myName.length; x++){
	if (
		myName[x].toLowerCase() == "a" ||
		myName[x].toLowerCase() == "e" ||
		myName[x].toLowerCase() == "i" ||
		myName[x].toLowerCase() == "o" ||
		myName[x].toLowerCase() == "u"
		) {
		console.log(3)
	}else{
		console.log(myName[x]).toLowerCase()
	}
};*/

// Continue and Breaj Statenets
/*
	the "continue" allows the code to go to the next iteration of the loop without finishing the execution of all statements in the block

	the "break" is used to terminate the current loop once a match has been found/the condition returns true
*/
for(let count = 0; count<=20;count++){
	if (count % 2 === 0){
		// tells the code to continue to the next iteration of the loop; it will ignore all preceding of the block
		continue;
	};
	// of the remainder is not equal to zerom this will be executed
	console.log("Continue and Break: " + count);

	if (count > 10){
		// tells the code to terminate/stop loop even if tge expression/condition of loop will return true for the next iteration.
		break;
	};
};

// mini-activity

let name = "alexandro"
for (x = 0; x<name.length; x++){
	console.log(name[x]); //will display "a" and "d" in the string
	if(name[x]==="a"){
		continue;
	};
	// console.log(name[x]); - will not display all of the "a" in the string
	if(name[x]==="d"){
		break;
	}
	// consol.log(name[x])l - will not display all of the "a" and "d" in the string
}

for (let x=0;x<=3;x++){
	for(let y=0;y<=x;y++)
		console.log("x:"+x+"y:"+y)
}
